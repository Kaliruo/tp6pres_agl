package pres;

import java.util.ArrayList;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class BibliUtilities {
	private GlobalBibliographyAccess gbibAccess; 
	private Clock clock; //Clock au lieu d'une localdate car elle est mockée

	
	//Cherche les notices avec le même auteur que la notice prise en paramètre et retourne une ArrayList avec 5 au maximum de ces notices
	//On ne peut pas avoir la même notice que celle en paramètre dans l'arrayList
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref){
		ArrayList<NoticeBibliographique> noticesCon=new ArrayList<>();
		ArrayList<NoticeBibliographique> auteurs= gbibAccess.noticesDuMemeAuteurQue(ref); //Arraylist de notices avec le même auteur que ref
		int i=0;
		while (noticesCon.size()<5 && auteurs.size()>i) { // Vérifie que la liste des notices connexes ne dépasse pas 5 et que celle des auteurs ne soit pas vide
			if (auteurs.get(i).getIsbn()!=ref.getIsbn() && !(noticesCon.contains(auteurs.get(i)))) { // Vérifie que l'ISBN soit différent et que l'AL ne contienne pas déjà la même notice
				noticesCon.add(auteurs.get(i));
			}
			i++;
		}
		return noticesCon;
	}
	
	
	//Rend le status de la notice(ISBN pris en paramètre), parmi les différentes énumérations de NoticeStatus
	//Vérifie l'existance de la notice et l'ajoute dans le cas contraire dans la bibliothèque
	//Jette une énumération dans le cas ou l'ISBN mis en paramètre est invalide
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException{
		try {
			NoticeBibliographique notice = gbibAccess.getNoticeFromIsbn(isbn);
			return Bibliothèque.getInstance().addNotice(notice);
		} // si ISBN invalide :
		catch (IncorrectIsbnException e) {
			throw new AjoutImpossibleException();
		}
	}
	
	
	//Vérifie si le dernier inventaire à été fait dans les 12 mois, sinon l'inventaire doit être fait et renvoie True
	public boolean prevoirInventaire() {
		return Period.between(Bibliothèque.getInstance().getLastInventaire(),LocalDate.now(clock)).toTotalMonths()>=12; //LocalDate.now() pour transformer la clock en localdate
	}
	
	//Constructeur de la classe
	public BibliUtilities(GlobalBibliographyAccess gbibAccess, Clock clock) {
		this.gbibAccess=gbibAccess;
		this.clock=clock;
	}

}
