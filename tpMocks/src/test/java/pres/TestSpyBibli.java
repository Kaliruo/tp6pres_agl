package pres;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;

class TestSpyBibli {
	// Les spies sont aussi appelés mock partiels
	
	// Création d'une instance espionne de classe bibliothèque, càd, que l'on peut alterer certains de ses comportements 
	Bibliothèque b = spy(Bibliothèque.getInstance());
	//Creation d'une instance espionne de la classe noticeBibliographique
	NoticeBibliographique n1= spy(new NoticeBibliographique("0_1","L'éveil","Line Papin"));
	@Spy
	NoticeBibliographique n2 = new NoticeBibliographique("0_2","Petit ours Brun","Claude Lebrun");
	//Test classique et test modifié avec la méthode getLastInventaire()
	@Test
	void testLastInventaire() {
		assertEquals(LocalDate.of(2020,12,07),b.getLastInventaire());
		when(b.getLastInventaire()).thenReturn(LocalDate.of(2021, 05, 18));
		assertEquals(LocalDate.of(2021, 05, 18), b.getLastInventaire());
	}

	//Test classique et test modifié avec la méthode getAuteur()()
	@Test
	void testNoticeBiblio(){
		assertEquals("Line Papin", n1.getAuteur());
		when(n1.getAuteur()).thenReturn("Frédéric Delavier");
		assertEquals("Frédéric Delavier", n1.getAuteur());
	}
	/*
	//Test classique et test modifié avec la méthode addNotice()
	@Disabled //Rend updated au lieu de newlyAdded
	@Test
	void testAddtNotice() {
		assertEquals(b.addNotice(n1), NoticeStatus.newlyAdded);
		when(b.addNotice(n2)).thenReturn(NoticeStatus.updated);
		assertEquals(b.addNotice(n2),NoticeStatus.updated);
	}*/
}
