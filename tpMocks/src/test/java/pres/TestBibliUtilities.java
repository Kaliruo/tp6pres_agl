package pres;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeAll;

import java.time.Clock;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)

class TestBibliUtilities {
	
	//Création d'une bibliothèque via la méthode getInstance()
	Bibliothèque b= Bibliothèque.getInstance();
	
	//Créaton d'une instance mocké de la classe GlobalBibliogtraphyAccess
	@Mock
	GlobalBibliographyAccess bibAccessMock;
	
	//Création d'une horloge mocké
	@Mock
	Clock mockedClock;
	
	//Injections des instant mocké crée précedement dans l'instance bibiUtiMock de la classe BibliUtilities
	@InjectMocks
	BibliUtilities bibUtiMock;

	//Tests sur chercherNoticesConnexes :
	@Test
	void testNoticesCon() throws IncorrectIsbnException{

		//Création d'une liste de notice ne contenant qu'une notice 
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre2","auteur1");
		L.add(notice1);
		//Simule la méthode noticesDuMemeAuteurQue avec la liste crée précédement pour obtenir son premier élément
		when(bibAccessMock.noticesDuMemeAuteurQue(notice2)).thenReturn(L);
		
		assertEquals(L.get(0), bibUtiMock.chercherNoticesConnexes(notice2).get(0));
	}
	
	//Test en mettant 5 notices dans la liste des notices du même auteur 
	@Test
	void testNbNoticesEg5() throws IncorrectIsbnException{

		//Création d'une liste de 5 notice ayant le même auteur
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre2","auteur1");
		NoticeBibliographique notice3 = new NoticeBibliographique("0_3","titre3","auteur1");
		NoticeBibliographique notice4 = new NoticeBibliographique("0_4","titre4","auteur1");
		NoticeBibliographique notice5 = new NoticeBibliographique("0_5","titre5","auteur1");
		NoticeBibliographique notice6 = new NoticeBibliographique("0_6","titre6","auteur1");
		L.add(notice2);
		L.add(notice3);
		L.add(notice4);
		L.add(notice5);
		L.add(notice6);
		//Simule la méthode noticesDuMemeAuteurQue() avec la liste précédente
		//Pour que la méthode chercherNoticesConnexes() fonctionne correctement
		when(bibAccessMock.noticesDuMemeAuteurQue(notice1)).thenReturn(L);
		
		assertEquals(5, bibUtiMock.chercherNoticesConnexes(notice1).size());
	}
	
	//Même test mais en mettant 3 notices dans la liste des notices du même auteur 
	@Test
	void testNbNoticesInf5() throws IncorrectIsbnException{

		
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre2","auteur1");
		NoticeBibliographique notice3 = new NoticeBibliographique("0_3","titre3","auteur1");
		NoticeBibliographique notice4 = new NoticeBibliographique("0_4","titre4","auteur1");
		L.add(notice2);
		L.add(notice3);
		L.add(notice4);
		
		
		when(bibAccessMock.noticesDuMemeAuteurQue(notice1)).thenReturn(L);
		
		assertEquals(3, bibUtiMock.chercherNoticesConnexes(notice1).size());
	}
	//Même test en mettant 6 notices dans la liste des notices du même auteur
	@Test
	void testNbNoticesSup5() throws IncorrectIsbnException{

		
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre2","auteur1");
		NoticeBibliographique notice3 = new NoticeBibliographique("0_3","titre3","auteur1");
		NoticeBibliographique notice4 = new NoticeBibliographique("0_4","titre4","auteur1");
		NoticeBibliographique notice5 = new NoticeBibliographique("0_5","titre5","auteur1");
		NoticeBibliographique notice6 = new NoticeBibliographique("0_6","titre6","auteur1");
		NoticeBibliographique notice7 = new NoticeBibliographique("0_7","titre6","auteur1");
		L.add(notice2);
		L.add(notice3);
		L.add(notice4);
		L.add(notice5);
		L.add(notice6);
		L.add(notice7);
		
		when(bibAccessMock.noticesDuMemeAuteurQue(notice1)).thenReturn(L);
		
		assertEquals(5, bibUtiMock.chercherNoticesConnexes(notice1).size());
	}
	
	//Test avec 3 notices avec le même ISBN
	@Test
	void testNoticesMemeISBN() throws IncorrectIsbnException{

		//Création d'une liste de 6 notices du même auteurs, mais avec 3 notices qui ont le même ISBN
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_1","titre2","auteur1");
		NoticeBibliographique notice3 = new NoticeBibliographique("0_1","titre3","auteur1");
		NoticeBibliographique notice4 = new NoticeBibliographique("0_1","titre4","auteur1");
		NoticeBibliographique notice5 = new NoticeBibliographique("0_5","titre5","auteur1");
		NoticeBibliographique notice6 = new NoticeBibliographique("0_6","titre6","auteur1");
		NoticeBibliographique notice7 = new NoticeBibliographique("0_7","titre6","auteur1");
		L.add(notice2);
		L.add(notice3);
		L.add(notice4);
		L.add(notice5);
		L.add(notice6);
		L.add(notice7);
		
		when(bibAccessMock.noticesDuMemeAuteurQue(notice1)).thenReturn(L);
		
		assertEquals(3, bibUtiMock.chercherNoticesConnexes(notice1).size());
	}
	
	//Test avec 2 notices identiques
	@Test
	void testNoticesMemeNot() throws IncorrectIsbnException{

		
		ArrayList<NoticeBibliographique> L = new ArrayList<NoticeBibliographique>();
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre1","auteur1");
		NoticeBibliographique notice3 = new NoticeBibliographique("0_2","titre1","auteur1");
		NoticeBibliographique notice6 = new NoticeBibliographique("0_5","titre5","auteur1");
		NoticeBibliographique notice7 = new NoticeBibliographique("0_7","titre7","auteur1");
		L.add(notice2);
		L.add(notice3);
		L.add(notice6);
		L.add(notice7);
		
		when(bibAccessMock.noticesDuMemeAuteurQue(notice1)).thenReturn(L);
		
		assertEquals(3, bibUtiMock.chercherNoticesConnexes(notice1).size());
	}
	
	//Tests sur ajoutNotice :
	
	//Test d'ajout d'une notice existante dans la bibliothèque  aux notices connues
	@Test
	void testAddNotice() throws AjoutImpossibleException, IncorrectIsbnException {
		NoticeBibliographique notice1 = new NoticeBibliographique("0_1","titre1","auteur1");
		NoticeStatus not1 = NoticeStatus.newlyAdded;
		
		when(bibAccessMock.getNoticeFromIsbn("0_1")).thenReturn(notice1);
		assertEquals(not1, bibUtiMock.ajoutNotice("0_1"));
	}
	
	//Test de modification d'une notice déjà connue
	@Test
	void testUpdatedNotice() throws AjoutImpossibleException, IncorrectIsbnException {
		NoticeBibliographique notice2 = new NoticeBibliographique("0_2","titre2","auteur2");
		NoticeStatus not2 = NoticeStatus.updated;
		
		when(bibAccessMock.getNoticeFromIsbn("0_2")).thenReturn(notice2);
		bibUtiMock.ajoutNotice("0_2");
		
		when(bibAccessMock.getNoticeFromIsbn("0_2")).thenReturn(new NoticeBibliographique("0_2","titre5","auteur2"));
		assertEquals(not2, bibUtiMock.ajoutNotice("0_2"));
	}
	
	//Test d'ajout d'une notices déjà connue
	@Test
	void testNoChangeNotice() throws AjoutImpossibleException, IncorrectIsbnException {	
		NoticeBibliographique notice3 = new NoticeBibliographique("0_3","titre3","auteur3");
		NoticeStatus not3 = NoticeStatus.nochange;
		
		when(bibAccessMock.getNoticeFromIsbn("0_3")).thenReturn(notice3);
		bibUtiMock.ajoutNotice("0_3");
		assertEquals(not3, bibUtiMock.ajoutNotice("0_3"));
	}
	
	//Test d'ajout d'une notice inexistante dans la bibliothèque  aux notices connues
	@Test
	void testNotExistNotice() throws AjoutImpossibleException, IncorrectIsbnException {
		NoticeStatus not4 = NoticeStatus.notExisting;
		
		when(bibAccessMock.getNoticeFromIsbn("0_4")).thenReturn(null);
		assertEquals(not4, bibUtiMock.ajoutNotice("0_4"));
	}
	
	//Test d'ajout d'une notice avec un ISBN non valide
	@Test
	void testExceptionNotice() throws IncorrectIsbnException {
		doThrow(IncorrectIsbnException.class).when(bibAccessMock).getNoticeFromIsbn("ab");
		//when(bibAccessMock.getNoticeFromIsbn("0_1")).thenThrow(new IncorrectIsbnException());
		assertThrows(AjoutImpossibleException.class, () -> {bibUtiMock.ajoutNotice("ab");
		});	
	}
	
	//Tests sur prevoirInventaire :
	
	//Test sur la méthode prevoirInventaire avec une différence entre la date du dernière d'inventaire
	//et la date de l'horloge mocké > à 12 mois
	@Test
	void testInventTrue() {
		LocalDate date = LocalDate.of(2022, 12, 01);
		
		Clock fixedClock = Clock.fixed(date.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(true,bibUtiMock.prevoirInventaire());

	}

	//Test sur la méthode prevoirInventaire avec une différence entre la date du dernière d'inventaire
	//et la date de l'horloge mocké < à 12 mois
	@Test
	void testInventFalse() {
		LocalDate date = LocalDate.of(2021, 12, 01);
		
		Clock fixedClock = Clock.fixed(date.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(false,bibUtiMock.prevoirInventaire());

	}
	

	
	
}
